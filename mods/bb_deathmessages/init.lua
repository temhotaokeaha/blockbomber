
bb_deathmessages = {}
bb_deathmessages.messages={}
bb_deathmessages.messages["explosion"] = {
    " was burnt to a crisp.",
    " is flat-out done.",
    " was pancaked by a small explosion.",
    " turned into small specks.",
    " is toast. Literally.",
    " tried biting off more than they could chew, and burnt their mouth.",
    " popped.",
    " was killed by death.",
    " learned explosions aren't powerups.",
    " terminated by THE GAME.",
    " lost their appitite.",
    " didnt think good.",
    " was slightly too slow.",
    " flew on a blast of hot air.",
    " might have lacked intellignce, but compensated with plenty of explosive.",
    ' made a small "pop" and disappeared.',
    " became a star!",
    " had a new experience.",
    " was struck by an interesting thought.",
    " tried, at least.",
    " realized their mistake.",
    " had a bright idea!",
    " was fired.",
    " will rest in pieces.",
    " underestimated their invincibility.",
    " accurately simulated sublimation.",
    " was fully oxidized.",
    " learned the hard way.",
    " reached escape velocity",
    " had an explosion of intelligence",
    " wanted to spectate.",
    " witnessed the Big Bang.",
    " caused an eruption",
    " doesnt feel like it anymore.",
    " went for a coffee.",
    " croaked.",
    " got too excited.",
    " felt slightly discombobulated.",
    " died a little on the inside.",
    " spontaneously combusted.",
    " lived the fast life.",
    " loves particles, I guess.",
    " went out with a bang.",
    " is flashy.",
    "'s hypothesis was incorrect.",
    " theory was disproved by The Universe.",
    " played with fire.",
    " was too eager.",
    " ate something hot.",
    " is well done.",
    " is ready.",
    " looked too far into the oven.",
    "; rest in peices.",
    " checked if the oven was warm enough.",
    " is creating a toxic atmosphere.",
    " is letting their mind wander again.",
    " failed fire safety class.",
    " has some spare parts now.",
    " is no longer with us.",
    " is at large.",
    " is experiencing a chemical imbalence.",
    " didnt know the game was hot potato.",
    " went out with a whimper AND a bang.",
    " didn't really need those organs anyway.",
    " is just fine ... honest!",
    " is having regrets already.",
    " will be sure to return the favor.",
    " decided to keep the game short and to the point.",
    " earned a much-needed break.",
    " was doing really well there for a while.",
    " wants a do-over.",
    " doesn't remember this part from the training video.",
    " is trying to any% their bucket list.",
    " forgot to take their body.",
    " is visiting many exotic places, simultaneously.",
    " turned out for the party.",
    " needs to keep their insides where they belong.",
    " opened up shop, and business is booming!",
    " went out with a bang.",
    " is *not* invincible, after all.",
    " suffered a minor case of death.",
    " popped off.",
    " suddenly discovered the meaning of life, the universe, and everything.",
    " is one with the universe.",
    " found out what happens when the bomber becomes the bombed."

}

bb_deathmessages.messages["falling"] = {

    " forgot to miss the ground.",
    " is off to new horizons.",
    " reached termnal velocity",
    " apparently had too much pride",
    " is flying. No wait, im looking at this backwards...",
    " believed they could fly.",
    " went down to the bottom.",
    " is still falling...",
    " is playing on a whole other level",
    " has embraced his deepest tendency",
    " saw a bird, or a plane, or something... anyhow they're gone now.",
    " went exploring.",
    " slipped.",
    " flopped.",
    " learned flying is for the birds.",
    " tried to find the surface.",
    ' says: "hey guys things look different from here! *zcchzz* "',
    ': "over and out"',
    " went down the rabbithole.",
    " overstepped his boundaries.",
    " zoomed waaay out.",
    " is reaching for the stars.",
    " went off the deep end.",
    " went skydiving.",
    " became smaller and smaller in the distance.",
    " is approching the speed of light.",
    ' says: "hey guys, what happened to the gravity? ... oh."',
    " sprouted wings.",
    " reached for the horizon and slipped.",
    " found the event horizon.",
    " entered the twilight zone.",
    " forgot their chute.",
    " has their head in the clouds.",
    " passed out.",
    " lacked a safety net.",
    " tried living on the edge.",
    " realized the meaning of gravity.",
    " didnt watch their step.",
    " forgot that there was no atmosphere in space.",
    " dident realize that total vacuum kills.",
    " was caught breaking the Law... Of Gravity.",
    " is boldly going where no one has gone before.",
    " is exploring the final frontier.",
    " is assuming a new perspective.",
    " failed to miss the ground.",
    " forgot to pack a parachute",
    " can't fly, and found out the hard way.",
    " is looking up ... with regret.",
    " fellout.",
    " has their feet planted firmly on the ... oh wait.",
    " was once mighty, but is fallen!",
    " thought the floor was invisible. FYI: its not.",
    " and the playing field have had a bit of a falling out.",
    " sailed through the air... AND DIED.",
    " wanted to see what's under the map.",
    " tried to noclip without fly privs.",
    " tried to noclip without fly privs.",
    " is testing the effect of gravity on a mass.",
    " is somewhere... over the rainbow.",
    " is measuring terminal velocity.",
    " likes the feeling of weightlessness.",
    " clearly flies more like a banana than an arrow.",
    " shouldn't have booked the cheap flight.",
    " wanted a window seat, but got more than they bargained for.",
    " confused personal and outer space.",
    " cures insomnia by lying down hard enough.",
    " is enjoying the breeze.",
    " finds it peaceful out here.",
    " de-orbited.",
    " contracted Kessler Syndrome.",
    " wonders if there eventually is a ground below.",
    " thought they were an admin.",
 
}

bb_player.register_on_dieplayer(function(p_name,cause)
    local player = minetest.get_player_by_name(p_name)
    if not player then return end
    -- get a good randomseed
    math.randomseed(os.time())

    --get the player's color
    local player_color = bb_playermodel.saved_colors[p_name]

    -- determine which kind of message to show
    local msg

    -- choose the msg

    if bb_deathmessages.messages[cause] then
        msg = bb_deathmessages.messages[cause][math.random(1,#bb_deathmessages.messages[cause])]
    end

    --send it
    minetest.chat_send_all("*** " .. minetest.colorize(player_color, p_name) .. msg)

end)


-- win messages

-- win messages

bb_loop.register_on_celebrate(function(winner)
    
    -- the chat message to send
    local cm = ">> "
    local player_color = bb_playermodel.saved_colors[bb_loop.winner]
    if player_color then
        cm = cm .. minetest.colorize(player_color,bb_loop.winner)
        
    else
        cm = cm .. "Nobody"
    end

    minetest.chat_send_all(cm .. " wins the game!")
    
end)