bb_powerup = {}
local path = minetest.get_modpath("bb_powerup")
dofile(path.."/items.lua")

bb_powerup.normal_speed = .5-- the speed players start at
bb_powerup.speed_boost = .2 -- the speed ech speed boost adds
bb_powerup.speed_snail = .25
bb_powerup.speed_super = 30



minetest.register_chatcommand( "additem" , {
    privs = {
        additem = true,
    },
    description = " additem <itemname> , adds the item to the world at your location (you will pick it up too)",
    func = function( name , param )
        local player = minetest.get_player_by_name( name ) 
        if not player then return end
        local pos = player:get_pos()

        minetest.add_item(pos, param)

    end,
})



bb_loop.register_playstep(function()

    for _, player in pairs(minetest.get_connected_players()) do
        local p_name = player:get_player_name()
        local p_meta = player:get_meta()
        if p_meta:get_string("status") == "playing" and bb_loop.gamestate == "running" then

            --%%%%%%%%%%%%%%%%%%
            --set player speed
            --%%%%%%%%%%%%%%%%%%

            local snail = false
            local superfast = false
            local inv = player:get_inventory()
            local num_speed_boosts = bb_player.get_count_in_inv(player,"bb_powerup:extra_speed")
            -- determine the value of the above variables


            if inv:contains_item("main", ItemStack("bb_powerup:slow")) then
                snail = true
            end
            if inv:contains_item("main", ItemStack("bb_powerup:superfast")) then
                superfast = true
            end

            -- if they have a slow modifier then set their speed to slow
            if snail then
                bb_player.speeds[p_name] = bb_powerup.speed_snail
            -- if they have a super fast modifier then set their speed to very fast
            elseif superfast then
                bb_player.speeds[p_name] = bb_powerup.speed_super
            -- else just set their speed based on the number of speed boosts they have collected
            else
                local normal_speed = bb_powerup.normal_speed --.75
                local speed_boost = bb_powerup.speed_boost -- .5
                local newspeed = normal_speed + num_speed_boosts * speed_boost
                bb_player.speeds[p_name] = newspeed
            end
            
            
            --%%%%%%%%%%%%%%%%%%
            --inventory sorting
            --%%%%%%%%%%%%%%%%%%
        
            local inv = player:get_inventory()
            local list = inv:get_list("main")
            
            local bombs = nil
            local power = nil
            local speed = nil
            local life = nil
            local throw = nil
            local kick = nil
            local multidir = nil
            local bad = nil
            local new_list = {}
            for _,stack in pairs(list) do
                if stack:get_name() == "bb_powerup:extra_bomb" then
                    bombs = stack                
                elseif stack:get_name() == "bb_powerup:extra_power" then
                    power = stack                
                elseif stack:get_name() == "bb_powerup:extra_speed" then
                   speed = stack                
                elseif stack:get_name() == "bb_powerup:extra_life" then
                    life = stack                
                elseif stack:get_name() == "bb_powerup:throw" then
                    throw = stack                
                elseif stack:get_name() == "bb_powerup:kick" then
                    kick = stack                 
                elseif stack:get_name() == "bb_powerup:multidir" then
                    multidir = stack                 
                elseif stack:get_name() ~= "" then
                    bad = stack 
                end
            end

            local counter = 1

            bb_player.clear_inv(player)
            
            if bombs ~= nil then
                inv:set_stack("main", counter, bombs)
                counter = counter + 1
            end
            if power ~= nil then
                inv:set_stack("main", counter, power)
                counter = counter + 1
            end
            if speed ~= nil then 
                inv:set_stack("main", counter, speed)
                counter = counter + 1
            end
            if life ~= nil then
                inv:set_stack("main", counter, life)
                counter = counter + 1
            end
            if throw ~= nil then
                inv:set_stack("main", counter, throw)
                counter = counter + 1
            end
            if kick ~= nil then 
                inv:set_stack("main", counter, kick)
                counter = counter + 1
            end
            if multidir ~= nil then 
                inv:set_stack("main", counter, multidir)
                counter = counter + 1
            end
            if bad ~= nil then
                inv:set_stack("main", counter, bad)
                counter = counter + 1
            end
            local hotbar = counter - 1

            player:hud_set_hotbar_itemcount(hotbar)
            player:hud_set_hotbar_image("bb_player_gui_hotbar_"..hotbar..".png")
        else
            player:hud_set_hotbar_itemcount(2)
            player:hud_set_hotbar_image("bb_player_gui_hotbar_2.png")
        end


        --%%%%%%%%%%%%%%%%%%
        --add wear to time limited powerups
        --%%%%%%%%%%%%%%%%%%

        local inv = player:get_inventory()
        local list = inv:get_list("main")
        --for k, v in pairs(list) do
        for  v = 1 , inv:get_size("main") do
            local stack = inv:get_stack("main", v)
            local i_name = stack:get_name()
            if i_name == "bb_powerup:superfast" or
                i_name == "bb_powerup:slow" or
                i_name == "bb_powerup:weird_control" or
                i_name == "bb_powerup:party" or
                i_name == "bb_powerup:no_placebomb" then

                stack:add_wear(bb_player.wear_amt)
                inv:set_stack("main", v, stack)
            end

        end


    
        --%%%%%%%%%%%%%%%%%%
        --collect nearby items
        --%%%%%%%%%%%%%%%%%%
        local pos = player:get_pos()

        local objs = minetest.get_objects_inside_radius(vector.add(pos,vector.new(0,.5,0)), 0.7)
        for k,v in ipairs(objs) do
            if v and not ( v:is_player() ) and v:get_luaentity() and v:get_luaentity().name == '__builtin:item' then
                local stack = ItemStack(v:get_luaentity().itemstring)
                local stack_name = stack:get_name()
                local goodgroup = minetest.get_item_group(stack_name, "good")
                local badgroup = minetest.get_item_group(stack_name, "bad")

                


                v:remove()


                -- play sounds
                if goodgroup == 1 then 
                    minetest.sound_play({
                        name = "yippee",
                        gain = 1.0,
                    }, {
                        pos = player:get_pos(),
                        max_hear_distance = 32,  -- default, uses an euclidean metric
                    }, true)
                end
                if badgroup == 1 then 
                    minetest.sound_play({
                        name = "eew",
                        gain = .1,
                    }, {
                        pos = player:get_pos(),
                        max_hear_distance = 32,  -- default, uses an euclidean metric
                    }, true)
                end

                -- if there is a bad powerup in the inv, remove it, whenever you get any powerup
                local list = inv:get_list("main")
                for l, m in ipairs(list) do
                    local stack = m
                    local i_name = stack:get_name()
                    if i_name == "bb_powerup:superfast" or
                        i_name == "bb_powerup:slow" or
                        i_name == "bb_powerup:weird_control" or
                        i_name == "bb_powerup:party" or
                        i_name == "bb_powerup:no_placebomb" then
    
                        stack:take_item()
                        inv:set_stack("main", l, stack)
                    end
                end

                if stack_name == "bb_powerup:resurect" then
                    -- resurrect all players!!!
                    bb_player.resurrect_all_players()                   
                
                elseif stack_name == "bb_powerup:throw" or
                    stack_name == "bb_powerup:kick" or
                    stack_name == "bb_powerup:extra_life" or
                    stack_name == "bb_powerup:multidir" then

                        -- if the item is one of the pernament powerups then only add the item if the player doesnt already have one
                        
                    if not(inv:contains_item("main", stack_name)) then
                        inv:add_item("main", stack)
                    end

                else
                    -- for everything else, we can just add the item
                    inv:add_item("main", stack)
                end
            end
        end


    end

end)



-- implement party powerup (players autoplace bombs whenever they can)

local partytimer = 0
minetest.register_globalstep(function(dtime)
    if bb_loop.gamestate ~= "running" then return end 
    partytimer = partytimer + dtime
    if partytimer > .5 then
        partytimer = 0
        for _, player in pairs( minetest.get_connected_players() ) do
            local inv = player:get_inventory()
            if inv:contains_item("main", "bb_powerup:party") then
                bb_player.try_to_place_bomb(player)
            end
        end
    end
end)


-- special other controls (weird_control)

controls.register_on_press(function(player, control_name)
    if not ( control_name == "left" or control_name == "right" or control_name == "up" or control_name == "down") then
        return
    end
    local inv = player:get_inventory()
    if inv:contains_item("main", "bb_powerup:weird_control") then
        local look = player:get_look_horizontal()
        player:set_look_horizontal(look - 3.14)
    end

end)

