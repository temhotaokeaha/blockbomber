
-- +1 powerups

minetest.register_craftitem("bb_powerup:extra_bomb",{
    description = "extra bomb",
    inventory_image = "bb_powerup_bomb.png",
    on_drop = function() return end,
    groups = {good = 1},
    
})

minetest.register_craftitem("bb_powerup:extra_life",{
    description = "extra life",
    inventory_image = "bb_powerup_extra_life.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

minetest.register_craftitem("bb_powerup:extra_speed",{
    description = "extra speed",
    inventory_image = "bb_powerup_fast.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

minetest.register_craftitem("bb_powerup:extra_power",{
    description = "extra power",
    inventory_image = "bb_powerup_power.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})


-- permanent powerups

minetest.register_craftitem("bb_powerup:throw",{
    description = "throw bombs",
    inventory_image = "bb_powerup_throw.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})


minetest.register_craftitem("bb_powerup:kick",{
    description = "kick bombs",
    inventory_image = "bb_powerup_kick.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})


minetest.register_tool("bb_powerup:multidir",{
    description = "multidir",
    inventory_image = "bb_powerup_multidir.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

-- neutral modifiers

minetest.register_craftitem("bb_powerup:resurect",{
    description = "resurect other players",
    inventory_image = "bb_powerup_resurect.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})


minetest.register_tool("bb_powerup:superfast",{
    description = "superfast",
    inventory_image = "bb_powerup_superfast.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})

-- bad modifiers

minetest.register_tool("bb_powerup:slow",{
    description = "slow",
    inventory_image = "bb_powerup_slow.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})


minetest.register_tool("bb_powerup:weird_control",{
    description = "weird_control",
    inventory_image = "bb_powerup_weird_control.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})


minetest.register_tool("bb_powerup:no_placebomb",{
    description = "no_placebomb",
    inventory_image = "bb_powerup_no_placebomb.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})


minetest.register_tool("bb_powerup:party",{
    description = "party: causes bad cases of bomb placement",
    inventory_image = "bb_powerup_party.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})

