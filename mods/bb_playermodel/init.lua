bb_playermodel = {}

local storage = minetest.get_mod_storage()

bb_playermodel.saved_colors = minetest.deserialize(storage:get_string("saved_colors")) or {} -- stores the color assigned to the player

function bb_playermodel.savecolors()
    storage:set_string("saved_colors",minetest.serialize(bb_playermodel.saved_colors))
end

bb_playermodel.textures = {} -- stores the textures that the player will have in-game (out-game, their textures are clear)

bb_playermodel.player_colors_avail = {
    "#472d3c",
    "#5e3643",
    "#7a444a",
    "#a05b53",
    "#bf7958",
    "#eea160",
    "#b6d53c",
    "#71aa34",
    "#397b44",
    "#3c5956",
    "#5a5353",
    "#dff6f5",
    "#8aebf1",
    "#28ccdf",
    "#3978a8",
    "#394778",
    "#39314b",
    "#564064",
    "#8e478c",
    "#cd6093",
    "#ffaeb6",
    "#f4b41b",
    "#f47e1b",
    "#e6482e",
    "#a93b3b",
    "#827094",
    "#4f546b",
}



minetest.register_entity("bb_playermodel:dead_player",{
    initial_properties = {
        physical = false,
        use_texture_alpha = true,        
        collisionbox = {-0.01, -0.01, -0.01, 0.01, 0.01, 0.01},
        visual = "sprite",
        textures = {"blank.png"},
        visual_size = {x = 0.01, y = 0.01, z = 0.01},
        static_save = false,
    },
    on_step = function(self, dtime, moveresult)
        
        self._timer = self._timer + dtime
        if self._timer > 4 and self._falling then
            self.object:remove()
        end
        if bb_loop.gamestate == "loading" then
            self.object:remove()
        end
        
    end,

    _timer = 0,
    _falling = false,

})





minetest.register_on_joinplayer(function(player)

    local p_name = player:get_player_name()

    local player_colors = bb_playermodel.player_colors_avail

    local randcolor = player_colors[math.random(1,#player_colors)]
    
    bb_playermodel.saved_colors[p_name] = bb_playermodel.saved_colors[p_name] or randcolor

    bb_playermodel.savecolors()
    
    bb_playermodel.textures[p_name] = {"white.png^[colorize:"..bb_playermodel.saved_colors[p_name]..":255","player_parts.png"}

    player:set_properties({
        visual = "mesh",
        mesh = "bb_character.b3d",
        textures = {"blank.png","blank.png"},
        visual_size = {x = 4, y = 4},
        collisionbox = {-0.4, 0.0, -0.4, 0.4, 0.9,0.4},
        stepheight = 0.3,
        eye_height = 1.3,
        physical = false,
        collides_with_objects = false,
    })

end)


-- globalstep

minetest.register_globalstep(function(dtime)
    for _, player in pairs(minetest.get_connected_players()) do

        local meta = player:get_meta()
        local p_name = player:get_player_name()


        --handle setting textures and invisibility

        local blank_textures -- should the player be invisible?
        if bb_loop.gamestate == "running" or bb_loop.gamestate == "celebrate" then
            blank_textures = false
        else
            blank_textures = true
        end
        if meta:get_string("status") == "dead" then
            blank_textures = true 
        end

        if blank_textures then
            player:set_properties({
                textures = {"blank.png","blank.png"},
            })
        else
            player:set_properties({
                textures = bb_playermodel.textures[p_name],
            })
        end


        -- handle setting animations

        local anim -- what anim should the player have, if any?
        if bb_loop.gamestate == "running" or bb_loop.gamestate == "celebrate" then
            if meta:get_string("status") == "playing" then
                local controls = player:get_player_control()
                -- if player is pressing movement keys
                if controls.up or controls.down or controls.left or controls.right then
                    anim = "walk"
                else
                    anim = "stand"
                end
            end
        end

        -- set the animations, if needed
        if anim then
            if anim == "walk" then
                player:set_animation({x=60,y=100}, 50, 0, true)
            end
            if anim == "stand" then
                player:set_animation({x=0,y=40}, 20, 0, true)
            end
        end


    end
end)




--dying stuff

bb_player.register_on_dieplayer(function(p_name,cause,death_pos,death_dir)
    
    local player = minetest.get_player_by_name(p_name)
    if not player then return end

    --local ded_pos = player:get_pos()
    local look_dir = player:get_look_horizontal()
    
    -- make the deadplayer indicator, falling is a misnomer

    local p_props = player:get_properties()

    local obj = minetest.add_entity( vector.add(vector.round(death_pos),vector.new(0,-.5,0)) ,"bb_playermodel:dead_player")
    local ent = obj:get_luaentity()

    obj:set_properties(p_props)
    obj:set_properties({
        textures=bb_playermodel.textures[p_name],
        physical = false,
    })
    obj:set_yaw(look_dir)

    -- set animations
    if cause == "falling" then

        obj:set_animation({x=110,y=150}, 20, 2, false)
        ent._falling = true

    else

        obj:set_animation({x=170,y=210}, 90, 2, false)
        ent._falling = false

    end

    


end)